
import java.util.ArrayList;

public class Main {

    static String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};

    static int[] hoursWorked = {35, 38, 35, 38, 40};

    static double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};

    static String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};

    static String searchPosition = "Caissier";

    public static void main(String[] args) {
        for (int i = 0; i < employeeNames.length; i++) {
            if (hoursWorked[i] <= 35) {
                System.out.println(employeeNames[i] + " : " + hoursWorked[i] * hourlyRates[i] + " PokéDollars");
            } else {
                double totalWages = (35 * hourlyRates[i]) + ((35 - hoursWorked[i]) * hourlyRates[i] * 1.5);
                System.out.println(employeeNames[i] + " : " + totalWages + " PokéDollars");
            }
        }

       /*  // Boucle FOR-EACH
       int i = 0;
        for(var employee : employeeNames) {
            String wages;
            if(hoursWorked[i]<=35) {
                wages = employee + " : " + hoursWorked[i]*hourlyRates[i] + " PokéDollars";
                i++;
                System.out.println(wages);
            } else {
                double totalWages = (35 * hourlyRates[i]) + ((35 - hoursWorked[i]) * hourlyRates[i] * 1.5);
                wages = employee + " : " + totalWages + " PokéDollars";
                i++;
                System.out.println(wages);
            }
        }*/

        ArrayList<String> employeePosition = new ArrayList<String>();
        for (int j = 0; j < employeeNames.length; j++) {
            if (searchPosition.equals(positions[j])) {
                employeePosition.add(employeeNames[j]);
            }
        }
        if (employeePosition.isEmpty()) {
            System.out.println("Aucun employé trouvé.");
        } else {
            System.out.println(employeePosition);
        }
    }
}